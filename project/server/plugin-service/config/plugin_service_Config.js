
'use strict';

const MessageConfigUpdate = require('../messages/message-config-update');
const GuidGenerator = require('z-abs-corelayer-cs/guid-generator');
const PluginService = require('z-abs-corelayer-server/plugin-service');


class Config extends PluginService {
  constructor() {
    super();
    this.config = new Map([
      ['authorization', {value: false, type: 'boolean', eTag: `"${GuidGenerator.create()}"`, where: Config.CLIENT_SERVER}]
    ]);
  }
  
  onInit() {
    this.done();
  }
  
  onExit() {
    this.done();
  }
  
  getConfig(name, where) {
    const config = this.config.get(name);
    if(config) {
      if('client' === where && Config.CLIENT_SERVER >= config.where) {
        return config.value;
      }
      else if('server' === where && Config.CLIENT_SERVER <= config.where) {
        return config.value;
      }
    }
  }
  
  setConfig(name, value) {
    const config = this.config.get(name);
    if(config) {
      config.value = value;
      if(Config.CLIENT_SERVER >= config.where) {
        this.sendMessageToPersistentStore(new MessageConfigUpdate(name, value), 'ConfigStore');
      }
      return true;
    }
    else {
      return false;
    }
  }
  
  handleRestApi(method, body, parameters, cbResult) {
    if('PATCH' === method) {
      if('object' !== typeof body) {
        cbResult(400, 'Bad Request', null, null);
      }
      const members = Reflect.ownKeys(body);
      const errorMembers = [];
      members.forEach((member) => {
        const config = this.config.get(member);
        if(!config) {
          errorMembers.push(member);
        }
      });
      if(0 !== errorMembers.length) {
        cbResult(404, 'No Found', null, null);
      }
      else {
        members.forEach((member) => {
          const value = Reflect.get(body, member);
          this.setConfig(member, value);
        });
        cbResult(204, 'No Content', null, null);
      }
    }
    else {
      cbResult(400, 'Bad Request', null, null);
    }
  }
}

Config.CLIENT = 0;
Config.CLIENT_SERVER = 1;
Config.SERVER = 2;

module.exports = Config;
