
'use strict';

const MessageBase = require('z-abs-corelayer-cs/message/message-base');


class MessageConfigUpdate extends MessageBase {
  constructor(name, value) {
    super();
    this.name = name;
    this.value = value;
  }
}

module.exports = MessageConfigUpdate;
